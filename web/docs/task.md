# `py_sentry` Tasks

An `IntervalService` is designed to be a framework rather than a toolbox. This difference between these is who manages the flow of control. In a framework, such as `IntervalService`, it is the framework that manages the flow of control and makes calls out to the task, whereas in a toolbox it is the task that manages the flow of control and make calls to the toolbox.

Being a framework means that its behavior is implemented by "plug-in" component. That component is referred to as a `Task` and to work with the `IntervalService` is must follow the principles discussed in this document.

Apart from the `__init__` function, all others are optional. A full set of functions can be found the the `nop_task.py` example.


## Instantiation

The `Task` itself is declared as a [Python entry point](https://packaging.python.org/en/latest/specifications/entry-points/) that _must_ support the following signature, as demonstrated by that in the `configurable_task.py` example:

        def __init__(self):
            """
            Creates an instance of this class.
            """
            self.__params: Optional[Dict[str, Any]] = None

(The `self.__params` will be used later to store the current configuration parameter and will be common to many implementation of a `Task`.)

The `__init__` function is the one required function. All other methods are optional, though in most cases the following signature should also be provided in order for the `Task` to do anything, again demonstrated by that in the `configurable_task.py` example:

        def execute(self) -> None:
            """
            Executes the responsibilities of this executable
            """
            if None is not self.__params:
                for key, value in self.__params.items():
                    if None is value:
                        logging.info('Option "%s" has no value', key)
                    else:
                        logging.warning('Option "%s" has value "%s"', key, value)

(As should be obvious this implementation simply prints out the contents of the `self.__params` variable.)


## Configuration

However before the `execute` method is called the `Task` needs to configure itself, and this is what most of the functions of a `Task` implementation do. A Discussion of why each type on configuration is requires take place [elsewhere](raison_d_être.md#configuration), sufice to say there are three mechanisms for setting configuration parameters.

* Command line options
* Environmental variables
* Configuration file

This is also the basic precedence of each type of configuration, with the Command Line overruling the other two and the Environmental Variables overruling those in a Configuration File. More detail on this can be found [elsewhere](raison_d_être.md#configuration_precedence)


### Command Line Options

To support setting configuration parameters using Command Line options, a `Task` needs to build an `argparse.ArgumentParser` instance. This is done by implementing the following signature, as demonstrated by that in the `configurable_task.py` example:

Here is an example taken from the 

    from sentry import Config

    CONFIG_FILE_KEY = "config_file"
    CONFIG_SECTION_KEY = "ini_section"
    OPTION_ONE_KEY = "one"
    OPTION_THE_OTHER_KEY = "other"

    _DEFAULTS: Config = {
        OPTION_ONE_KEY: "default_for_one",
    }

    ...

        def create_argument_parser(self) -> argparse.ArgumentParser:
            """
            Creates and populates the argparse.ArgumentParser for this Task.
            """
            parser = argparse.ArgumentParser(
                description=f"Executes an instance of the {type(self).__name__} class"
            )
            parser.add_argument(
                "-a",
                f"--{OPTION_ONE_KEY}",
                dest="OPTION_ONE",
                help='An option that has a default that is "'
                + str(_DEFAULTS[CONFIG_FILE_KEY])
                + '"',
            )
            parser.add_argument(
                "-b",
                f"--{OPTION_THE_OTHER_KEY}",
                dest="OPTION_THE_OTHER",
                help="The other option, that does not have a default",
            )
            parser.add_argument(
                "-i",
                f"--{CONFIG_FILE_KEY}",
                dest="CONFIG_FILE",
                help="The path to the configuration file",
            )
            parser.add_argument(
                "-s",
                f"--{CONFIG_SECTION_KEY}",
                dest="CONFIG_SECTION",
                help="The section of the INI file to use for this execution",
            )
            return parser

It is important to note here that the `default` option for the `add_argument` call is _not_ used. Rather any default value is declared in the `_DEFAULTS` variable, which should be returned by the following signature:

        def get_defaults(self) -> Optional[Config]:
            """
            Returns a Config containing the default for every config item that has one.
            """
            return _DEFAULTS

Where the key within the returned `Config` is the key used to reference the parameter. The convention is that this key is the same as the "long" option name for the parameter, thought if this does not match the needs of the implementation this can be changed, but it is not advised.


The `IntervalService` calls the `get_defaults` function to set up the correct default values rather than using the `add_argument` feature as there is a variety of ways to set a value.


### Environmental Variables

To support setting configuration parameters using environmental variables, a `Task` needs to implement the following signature, as demonstrated by that in the `configurable_task.py` example:

        def get_envar_mapping(self) -> Dict[str, str]:
            """
            Return a dictionary that maps environmental variables onto option variables.
            """
            return {
                "CONFIG_CONFIG_FILE": "CONFIG_FILE",
                "CONFIG_CONFIG_SECTION": "CONFIG_SECTION",
                "CONFIG_OPTION_ONE": "OPTION_ONE",
                "CONFIG_OPTION_THE_OTHER": "OPTION_THE_OTHER",

In this case the returned dictionary contains the value of the `dest` option is the appropriate `add_argument` call, keyed by the environmental variable which, if it exists, hold that value for the parameter.


### Configuration File

To support setting configuration parameters using a configuration file, a `Task` needs to implement the following two signatures, as demonstrated by that in the `configurable_task.py` example.

        def get_config_file_key(self) -> Optional[str]:
            """
            Returns the name of the config file to use, if any.
            """
            return CONFIG_FILE_KEY
    
        def get_config_section_key(self) -> Optional[str]:
            """
            Returns the section of the config file to use, if any.
            """
            return CONFIG_SECTION_KEY

As can be seen the values return match the keys used to reference those parameters.

The configuration file, for the moment, is an [INI formated file](https://en.wikipedia.org/wiki/INI_file) which boils down to key-value pairs collected together in sections. By default the key for each parameter in this file is the lower-case version of the "long" option name and this mapping is build automatically. If the automatic mapping does not match the needs of the implementation this can be changed, but it is not advised.


### Using a Configuration

Once all of the ways a configuration can be established have been executed, it now becomes time to pass the resulting parameters into the `Task`. The first step in this operation is for the `IntervalService` to inform the `Task` implementation that a new or updated configuration is about to be set. This is done by implementing the following signature, still demonstrated by that in the `configurable_task.py` example:

        def updated_configuration(self, _: Config) -> None:
            """
            Informs this instance of the contents of the current configuration file. This can be
            used to construct the return value of 'get_param_names'.
            """
            self.__params = None
            logging.info("The configuration been updated")

In this example, the local copy of the configuration parameters is cleared.

The nest step is for the `Task` is to imform the `IntervalService` of those parameters in which is has interest. This is done by implementing the following signature, still demonstrated by that in the `configurable_task.py` example:

        def get_param_names(self) -> List[str]:
            """
            Returns a List of the config items that should be included when building the parameters
            passed in to 'set_params'. If this in None, 'set_params' will not be called.
            """
            return [
                OPTION_ONE_KEY,
                OPTION_THE_OTHER_KEY,
            ]

Note that the `CONFIG_FILE_KEY` and `CONFIG_SECTION_KEY` variables are not included in the returned list as this `Task` implementation does not use it, only the `IntervalService` does.

The final step is for the `IntervalService` to inform the `Task` implementation of the values for the parameters in the new configuration. This is done by implementing the following signature,

        def set_params(
            self, params: Dict[str, Any]
        ) -> Optional[str]:
            """
            Informs the Task of the new set of parameters it should use.
    
            Returns:
                None if the parameters values are valid, otherwise it is the reason why
                configuration failed.
            """
            if OPTION_ONE_KEY in params and 0 > params[OPTION_ONE_KEY]:
                return f"{OPTION_ONE_KEY} must be greater or equal to zero"
            self.__params = params
            logging.info("The new set of parameters has been set")
            return None

As can be seen in this example, this is the place where the validity of any parameter can be checked and, if necessary, rejected.

## Logging Behaviour

The logging behavior of the `IntervalService` implementation is controlled by two signatures, namely those shown below, as demonstrated by that in the `log_task.py` example.

    from sentry import Config, LOG_LEVELS, LOG_FILE_KEY, LOG_LEVEL_KEY

    ...

        def get_log_file_key(self) -> Optional[str]:
            """
            Returns the name of the log file to use, if any.
            """
            return LOG_FILE_KEY
    
        def get_log_level_key(self) -> Optional[str]:
            """
            Returns the name of the log level to use, if any.
            """
            return LOG_LEVEL_KEY

The `LOG_FILE_KEY` and `LOG_LEVEL_KEY` values are taken from the `sentry` module to encourage uniformity of the various `Task` implementations but it is not a requirement.

One other item of note in this example is that the `LOG_LEVELS` constant is imported from the `sentry` module in order to list the available levels in the help returned by the command line. This is done using the following snippet, taken from the `log_task.py` example, when populating the `ArgumentParser`.

            parser.add_argument(
                "-l",
                f"--{LOG_LEVEL_KEY}",
                dest="LOG_LEVEL",
                help="The logging level for this execution",
                choices=LOG_LEVELS.keys(),
            )


## Looped Execution

The main purpose of the `IntervalService` is to run the following signature repeatedly.

        def execute(self) -> None:
            """
            Executes the responsibilities of this executable.
            """

To manage the loop running this function, the `Task` can provide the three following signatures:

        def get_interval(self) -> Optional[float]:
            """
            Returns the number of seconds to wait between executions. None or a negative number
            means that the executions should stop.
            """
    
        def looping_starting(self) -> None:
            """
            Informs the Task that the execution loop is about to start.
            """
    
        def looping_stopped(self) -> None:
            """
            Informs the Task that the execution loop has stopped.
            """

The `get_interval` function returns the time, in second that the `IntervalService` instance should wait until invoking the `execute` function again. A value of less that zero or `None` means that theis instance of the loop should be terminated.

The `looping_starting` and `looping_stopped` functions provide an opportunity for the `Task` to set up and teardown any resources required while the loop is in progress.


### Controlling Looped Execution

While the `IntervalService` instance is looping it may be necessary to interrupt it. If this behavior is required then the `Task` can implement the following signature, as demonstrated by that in the `command_task.py` example.

    from sentry import COMMAND_KEY

        def get_command_key(self) -> Optional[str]:
            """
            Returns the config item containing the name of requested command.
            """
            return COMMAND_KEY

So with the ligging `KEYS`, the `COMMAND_KEY` are taken from the `sentry` module to encourage uniformity of the various `Task` implementations but it is not a requirement.

The `get_command_key` dunction allows a second instance of the `IntervalService` to send a signal to the running instance to controll its execution. At present the `IntervalService` service supports the following commands:

* `stop`: Causes the running instance to clean terminate.
* `reload`: Cleanly terminates the current loop, rereads the configuration file, and starts a new loop using the updated configuration.

In order for the second execution to be able to locate the running one, they need to share a file in which the running process number is held. This is done using the following signature.

    from sentry import PID_FILE_KEY

        def get_pid_file_key(self) -> Optional[str]:
            """
            Returns config item containing the name of the pid file to use.
            """
            return PID_FILE_KEY


## Undocumented function

A Task run by this service _may__ provide the following functions, which are currently undocumented:

    get_config_ignore(self) -> Optional[List[str]]
        If no 'get_config_mapping' function is supplied, this will return a list of config items to
        ignore when building the parameters passed in to 'set_params'.
        
    get_config_mapping(self, parse: argparse.ArgumentParser) -> Optional[Dict[str, str]]
        Returns a Dict that maps config items onto option variables, if this function does not exist
        in the Task it will be automatically generated from the ArgumentParser.
