# Raison d'être for this Project

The purpose of this project is to provide a framework in which a python task can be easily made into  a micro-service that runs are regular intervals, with the contents of this project providing a much of needed boiler-plate as possible.

In this context a interval-service is standalone executable which is responsible for the execution of a single task within the context of a [OCI container](https://opencontainers.org/), i.e. it is deployed as a docker image. The expected lifecycle for such a service in that environment is simply:

* Instante the task
* Configure the task
* Repeatedly execute the task until requested to stop.

Experience has shown that it is also useful for the lifecycle to support the following:

* Temporarily stop and resume the repeated execution of the task without destroying the executing instance.
* For those implementations that pause between executions of the task, interrupt that pause so that the task and execute immediately.

The remaining sections of this document will discuss the necessary functions that may need to be provided by the implementation of a task so that it can make the best use of the boiler-plate implementations provided by this project.


## Instantiation

This project is designed to be a framework rather than a toolbox. This difference between these is who manageds the flow of control. In a framework, such as this one, it is the framework that manages the flow of control and makes calls out to the task, whereas in a toolbox it is the task that manages the flow of control and make calls to the toolbox.

Being a framework means that it is in control of the instantiation of the task's implementation. This is done using the [python entry point mechanism](https://packaging.python.org/en/latest/specifications/entry-points/). The upshot of this choice is that the class implementing the task must have an initializer function with the following signature:

    def __init__(self) -> None :


## Configuration

There are a number of ways to configure a interval-service, this project supports the following three.

* Command line options

* Environmental variables

* A configuration file


### Command Line Options

During development of a service, it is usually run as a command. Therefore the easiest way to configuration it as this point is using command line options. For this purpose a task implementation should implement the `create_argument_parser` method that will return an `argparse.ArgumentParser` instance to be used by the framework to parse the command line.

The results of parsing the command line are returned to the task implementation by calling the `set_params` function.


### Environmental Variables

The next step in development is deploying the service in an OCI container. In this case the easiest way to declare configuration setting is to use environmental variables in the the container's deployment description. Moreover, in may cases different deployments of a service may only differ by a few settings, in which case the common settings can be contained in a configuration file (see next section) and environmental variables can be used to declared those settings that differ between deployments. For this purpose a task implementation should implement the `get_envar_mapping` method that will return a `Dict[str. str]` instance that maps environmental variables onto option variables that are defined in the `argparse.ArgumentParser` instance returned by the `create_argument_parser` method.


### Configuration File ###

For production deployment it is important to capture the configuration settings of a service. Moreover, as noted in the previous section, multiple deployment of a service may share many setting. In these cases having all of the settings gathered into a file simplifies deployment. For this purpose a task implementation should implement the `set_current_configuration` method that will take a `Dict[str, Union[bool, int, str, None]]` instance that contains the contents of most recently read configuration file.

The configuration file itself should be formated so that it can be read by the `configparser.ConfigParser` class. The filename and section of that file to be used by the service may be defined either on the command line or by one or more environmental variables. As the option variables for both of those methods are decided by the task implementation, that means that the  implementation must implement the `get_config_file_option` and `get_config_section_option` to return the name of the appropriate option variables. The framework will then use these to read, parse and then invoke the `set_current_configuration`.


### Configuration Precedence ###

Given the variety of ways to supply configuration settings, it is important to and a clear and deterministic algorithm by which the setting to be used can be evaluated. Any service implementation should conform to this algorithm.

The algorithm is as follows:

* Any setting on the command line takes precedence over any other.

* Any environmental variable is used as the default value for a command line option. Thus if the option appears in the command line is will overwrite the environmental variable value.

* Configuration file settings are last in order of precedence. However if they are not overridden by a command line option nor an environmental variable these setting, and only these settings, can be changed while a service is running by sending it a SIGHUP signal that will re-read the configuration file and call `set_current_configuration` before returning control back to the service.

To help enforce this algorithm, a service implementation is passed the set of command line options, including any environmental variable that have been set but not superseded, at creation time. The implementation should remember all of the settings that are contained in the passed options so that settings read from a configuration file does not override them. In conjunction with this, setting contained within configuration file are passed to the service implementation, after it has been created, by invoking the `set_current_configuration` method, which may be invoked more that once, and during this method only settings not set by the original options object may be changed.


## Execution ##

The framework provides a basic loop in which the function implementing the task can be invoked. This function must have the following signature.

    def execute(self) -> bool:

The loop itself in controlled by the framework, so the return value of the function is used to signal to the framework whether loop should continue or not. `True` means the loop should continue, while `False` means the whole service should terminate.

The task is not the only way, or even the normal way, that the loop is controlled. The framework is responsible for 

The start and stop of looping  
The loop itself in controlled by the framework, but the task may implement either of the following  functions the the framework will call at the appropriate time.

    def start_looping(self) -> None:
    def stop_looping(self) -> None:

If, for some reason, the task wants to maintain control of the loop that is possible by looping in the `run` function and using the `start_looping` and `start_looping` function to set up the appropriate flags within the task's implementation.

